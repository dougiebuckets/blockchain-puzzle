pragma solidity ^0.4.17;

import "./Ownable.sol";
import "./safemath.sol";

contract UserFactory is Ownable {  

    // @dev Definition of User struct and its grouped list of variables
    using SafeMath for uint;

    struct User {
        string name;
        uint level;
    }

    // @dev Not sure if the front-end will use events; functions are returning booleans at the moment
    // event IncrementUserLevel(address indexed from);

    // @dev An array containing the User struct for all Users
    User[] public users;

    // @dev A mapping of userId's to their associated owner addresses
    mapping(uint => address) public userToOwner; 

    // @dev A mapping of user addresses and their associated levels in the game
    mapping(address => uint) public userLevel; 

    // @dev A mapping of levels to the associated number of users at a particular level
    mapping(uint => uint) levelToNumberOfUsers;

    // @dev Saving this in case we need it later
    // uint id = users.push(User(level)) -1;

    function createUser(string name) public {
        // @dev Set user level to 1 when initialized
        userLevel[msg.sender] = 1;
        uint level = currentLevel(msg.sender); 

        // @dev Push user into User array
        uint id = users.push(User(name, level)) - 1;

        // @dev Set id key type to user address key value
        userToOwner[id] = msg.sender;

        // @dev Increment the number of users at level 1
        levelToNumberOfUsers[level]++;
    }

    // @dev This function will advance a user to the next level
    function incrementLevel() internal {
        // IncrementUserLevel(msg.sender); 
        userLevel[msg.sender]++;

        uint level = currentLevel(msg.sender); 

        // @dev Increment the number of users at this level when called
        levelToNumberOfUsers[level]++;
    }

    // @dev Decrement the number of users at this level when called
    function removeFromPriorLevel() internal {
        uint level = currentLevel(msg.sender); 
        levelToNumberOfUsers[level]--;
    }

    // @dev This function is used to find the level a user is currently on 
    // @param This function takes in a user's address as the argument
    function currentLevel(address userAddress) public constant returns (uint) {
        return userLevel[userAddress];
    }

    // @dev This function returns the number of users at a particular level 
    // @param This function takes in a user's level as the argument
    function numberOfUsersPerLevel(uint level) public constant returns (uint) {
        return levelToNumberOfUsers[level];
    }

    // @dev Return an address[] array with all the user addresses a level has
    function usersInLevel(uint _level) external view returns (address[]) {
        address[] memory result = new address[](levelToNumberOfUsers[_level]);
        uint counter = 0;
        for (uint i = 0; i < users.length; i++) {
            if(users[i].level == _level) {
                result[counter] = userToOwner[i];
                counter++;
            }
        }
        return result;
    }

}
